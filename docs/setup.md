# Setup Instructions

In order to run the demo successfully, you need to set up one permanent group and then a temporary project for each demo.

## Group

This will be the group containing all your demo projects.

1. Set up your GitLab instance or use gitlab.com. As a partner, you are eligible to get an [NFR license](https://handbook.gitlab.com/handbook/resellers/channel-working-with-gitlab/#requesting-a-gitlab-nfr-not-for-resale-license) for demo purposes. For setting up a self-managed GitLab instance, please refer to our [Reference Architectures documentation](https://docs.gitlab.com/ee/administration/reference_architectures/) and/or use the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit).

2. Create a group for demos.

3. Import the [template project](https://gitlab.com/gitlab-partners-public/vulnerable-express) to the group (or a subgroup of it), using the "Repository by URL" option. (Make sure you use [the right URL](https://gitlab.com/gitlab-partners-public/vulnerable-express.git).) **After** the import finishes, turn on repo mirroring. (Doing it during the import is broken right now.) This will keep this repo in sync with the original template project. Use this imported project as a template for later demos by forking it. (On gitlab.com you can just fork the original template project, and you don't need to import and mirror it at all. But this step of the instructions is aimed to be general, regardless of what instance of GitLab you are using to do a demo.)

4. On the group level, enable shared runners on gitlab.com. If you are self-managing your GitLab instance or don't want to/can't use the shared ones, [set up your own runners](https://docs.gitlab.com/runner/) for this group.

5. Connect a Kubernetes cluster to the demo group. This can be done in many ways. One option:
    1. [Set up a Kubernetes cluster on GCP with the GitLab Kubernetes agent installed](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html)
    2. Enable Ingress for the cluster: [step 1](https://docs.gitlab.com/ee/user/clusters/management_project_template.html); [step 2](https://docs.gitlab.com/ee/user/infrastructure/clusters/manage/management_project_applications/ingress.html)

6. [Enable Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/#get-started-with-auto-devops) on the group level. Please make sure you satisfy all the requirements listed [here](https://docs.gitlab.com/ee/topics/autodevops/requirements.html). Pay extra attention to the [base domain setup](https://docs.gitlab.com/ee/topics/autodevops/requirements.html#auto-devops-base-domain).

## Project

This project will be the one you'll use to run the demo. This can be deleted after the demo is over.

Make sure you set up the project **at least one day** before the demo, because:

* The Auto DevOps pipelines must finish (it takes around 10 minutes), and the app is deployed into the Kubernetes cluster by the time you start the demo.

* The [security dashboard update](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#view-vulnerabilities-over-time-for-a-project) must pass. It happens every day at 01:15 UTC.

The following setup steps need to be done for each new demo (approx. 5 mins):

1. Fork the template project.

2. Remove fork relationship ([otherwise you can't create an MR from an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/15598)).

3. Settings > Merge requests > Enable 'Pipelines must succeed'.

4. Make sure Auto DevOps is enabled. (It should be, because of group settings above.) If needed, kick off a pipeline manually for the `master` branch.

5. Create a new label: `doing`.

6. Add `doing` list to the issue board.

7. Create a new issue in the `doing` list with the title "Redesign colors". Set its description:
```
- [x] Change text color to white
- [x] Change background color to orange
````

8. Assign the issue to yourself.

9. Create an MR from the issue. Assign it to yourself.

10. In the MR: Code/Open in Web IDE. Go to: `public/stylesheets/style.css`. Add to `body`:
```
color: white;
background: orange;
````

11. Commit message: "Change colors". Commit & push.
